
mkdir -p ~/wilselby_ws/src
cd ~/wilselby_ws/src && git clone https://github.com/PX4/mav_comm.git
cd ~/wilselby_ws/src && git clone https://github.com/ethz-asl/glog_catkin.git && cd glog_catkin && git checkout 314b53e
cd ~/wilselby_ws/src && git clone https://github.com/catkin/catkin_simple.git
cd ~/wilselby_ws/src && git clone https://github.com/wilselby/ROS_quadrotor_simulator
cd ~/wilselby_ws/src && git clone https://github.com/wilselby/rotors_simulator
cd ~/wilselby_ws/src && git clone https://github.com/ethz-asl/gflags_catkin

rosdep update
cd ~/wilselby_ws && rosdep -y install --from-paths src --ignore-src --rosdistro indigo

echo 'add_dependencies(action_controller action_controller_generate_messages_cpp)' >> ~/wilselby_ws/src/ROS_quadrotor_simulator/action_controller/CMakeLists.txt
cd ~/wilselby_ws && catkin_make -j 1 || true
source ~/wilselby_ws/devel/setup.sh && cd ~/wilselby_ws && catkin_make -j 1
