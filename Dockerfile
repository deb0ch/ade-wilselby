
# An ADE-compatible image
FROM registry.gitlab.com/deb0ch/ade-ubuntu/trusty-extra:latest


##
##  Install ros-indigo and dependencies
##

RUN apt-get update && apt-get -y install \
    git                                  \
    gnupg2                               \
    lsb-release                          \
    unzip                                \
    && rm -rf /var/lib/apt/lists/*

RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros-latest.list'

RUN apt-get update && apt-get -y install \
    autoconf                             \
    libgflags-dev                        \
    libgoogle-glog-dev                   \
    python-catkin-tools                  \
    python-rosinstall                    \
    ros-indigo-desktop-full              \
    ros-indigo-geodesy                   \
    ros-indigo-joy                       \
    ros-indigo-move-base                 \
    ros-indigo-moveit-core               \
    ros-indigo-moveit-kinematics         \
    ros-indigo-moveit-msgs               \
    ros-indigo-moveit-planners-ompl      \
    ros-indigo-moveit-ros-move-group     \
    ros-indigo-moveit-ros-visualization  \
    ros-indigo-octomap-msgs              \
    ros-indigo-octomap-ros               \
    && rm -rf /var/lib/apt/lists/*

RUN sh -c 'echo "deb http://packages.ros.org/ros-shadow-fixed/ubuntu/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros-shadow.list'

RUN apt-get update && apt-get -y install \
    ros-indigo-mavros                    \
    ros-indigo-mavros-extras             \
    && rm -rf /var/lib/apt/lists/*

RUN rosdep init


##
## Install gazebo 2
##

RUN apt-get update && apt-get -y install \
    gazebo2                              \
    && rm -rf /var/lib/apt/lists/*

ENV GAZEBO_MODEL_PATH=/usr/share/gazebo/models

RUN git clone --depth=1 https://github.com/osrf/gazebo_models $GAZEBO_MODEL_PATH \
    && rm -rf $GAZEBO_MODEL_PATH/.git


##
## ADE
##

COPY env.sh /etc/profile.d/ade-wilselby_env.sh


##
## Copy local files into the system
##

COPY skel/. /
